import dao.DefaultPersonDao;
import dao.PersonDao;
import dto.Employer;
import entity.Person;
import exception.NotPersonException;
import exception.PersonNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import service.DefaultPersonService;
import service.PersonService;
import storage.TemporaryStorage;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class Test {

    @InjectMocks
    private PersonService personService = new DefaultPersonService();

    @Mock
    private PersonDao personDao;

    private Employer employer;
    private Person person;
    private String id;
    private UUID uuid;
    private TemporaryStorage storage;
    private List<Person> peopleWithSameName;
    private List<Person> peopleWithSameAge;
    private List<Person> peopleWithSameNameAndAge;
    private List<Person> people;
    private List<String> ids = new ArrayList<>();
    private List<Employer> employers;

    @BeforeEach
    public void init() {
        employer = new Employer("Dima", "Pushkin", 30);

        uuid = UUID.randomUUID();
        id = UUID.randomUUID().toString();

        for (int i = 0; i < 5; i++) {
            ids.add(UUID.randomUUID().toString());
        }

        person = new Person("Alex", (byte) 25);

        employers = new ArrayList<>();
        employers.add(new Employer("Anna", "Harrison", 21));
        employers.add(new Employer("Sam", "Black", 23));
        employers.add(new Employer("Jon", "Kim", 34));
        employers.add(new Employer("Alex", "Y", 21));


        people = new ArrayList<>();
        people.add(new Person("Ivan", (byte) 21));
        people.add(new Person("Roman", (byte) 23));
        people.add(new Person("Oleg", (byte) 17));
        people.add(new Person("Alex", (byte) 17));

        peopleWithSameName = new ArrayList<>();
        peopleWithSameName.add(new Person("Roman", (byte) 21));
        peopleWithSameName.add(new Person("Roman", (byte) 23));

        peopleWithSameAge = new ArrayList<>();
        peopleWithSameAge.add(new Person("Kirill", (byte) 19));
        peopleWithSameAge.add(new Person("Nadya", (byte) 19));

        peopleWithSameNameAndAge = new ArrayList<>();
        peopleWithSameNameAndAge.add(new Person("Kirill", (byte) 19));
        peopleWithSameNameAndAge.add(new Person("Kirill", (byte) 19));

        storage = TemporaryStorage.getInstance();
    }

    @org.junit.jupiter.api.Test
    public void get() throws NotPersonException, PersonNotFoundException {
        when(personDao.getPerson(uuid.toString())).thenReturn(person);
        Person person = personService.get(uuid);

        assertNotNull(person);
    }

    @org.junit.jupiter.api.Test
    public void save() {
        when(personDao.savePerson(any(Person.class))).thenReturn(id);

        UUID uuid = personService.save(employer);
        assertNotNull(uuid);
    }

    @org.junit.jupiter.api.Test
    public void saveAll_sould_return_ids_seved_persons() {
        List<String> ids = personDao.saveAll(people);
        assertNotNull(ids);

        List<UUID> uuids = personService.saveAll(employers);
        assertNotNull(uuids);
    }

    @org.junit.jupiter.api.Test
    public void saveAll_should_throw_NullPointerException() {
        when(personDao.saveAll(isNull(List.class))).thenThrow(new NullPointerException());

        assertThrows(NullPointerException.class, () -> personService.saveAll(null));
    }


    @org.junit.jupiter.api.Test
    public void getByName_should_return_list_of_persons() throws PersonNotFoundException {
        String name = "Roman";
        when(personDao.findByName(name)).thenReturn(peopleWithSameName);

        List<Person> people = personService.getByName(name);
        assertNotNull(people);
    }


    @org.junit.jupiter.api.Test
    public void getByName_should_throw_PersonNotFoundException() throws PersonNotFoundException {
        when(personDao.findByName(null)).thenThrow(new PersonNotFoundException());

        assertThrows(PersonNotFoundException.class, () -> personService.getByName(null));
    }

    @org.junit.jupiter.api.Test
    public void getByAge_should_return_list_of_persons() throws PersonNotFoundException {
        byte age = 19;
        when(personDao.findByAge(age)).thenReturn(peopleWithSameAge);

        List<Person> people = personService.getByAge(age);
        assertNotNull(people);
    }


    @org.junit.jupiter.api.Test
    public void getByAge_should_throw_PersonNotFoundException_when_age_is_not_valid() throws PersonNotFoundException {
        byte age = -1;
        when(personDao.findByAge(age)).thenThrow(new PersonNotFoundException());

        assertThrows(PersonNotFoundException.class, () -> personService.getByAge(age));
    }


    @org.junit.jupiter.api.Test
    public void getByAgeAndName_should_return_list_of_persons() throws PersonNotFoundException {
        byte age = 19;
        when(personDao.findByNameAndAge("Kirill", age)).thenReturn(peopleWithSameNameAndAge);

        List<Person> people = personService.getByNameAndAge("Kirill", age);
        assertNotNull(people);
    }

    @org.junit.jupiter.api.Test
    public void getByAgeAndName_should_throw_PersonNotFoundException() throws PersonNotFoundException {
        byte age = -29;
        String names = "sdfgsdfg";
        when(personDao.findByNameAndAge(names, age))
                .thenThrow(new PersonNotFoundException());
        assertThrows(PersonNotFoundException.class, () -> personService.getByNameAndAge(names, age));

    }

    @org.junit.jupiter.api.Test
    public void getAll_soud_return_list_of_all_persons() throws NotPersonException {

        when(personDao.findAll()).thenReturn(people);

        List<Person> pers = personService.getAll();
        assertNotNull(pers);
    }

    @org.junit.jupiter.api.Test
    public void removeById_sould_return_true() throws PersonNotFoundException {
        Person man = person;
        when(personDao.deleteById(person.getId())).thenReturn(true);

        boolean deleted = personService.removeById(person.getId());
        assertTrue(deleted);
    }

    @org.junit.jupiter.api.Test
    public void removeById_sould_throw_PersonNotFoundExcepton() throws PersonNotFoundException {
        when(personDao.deleteById(null)).thenThrow(new PersonNotFoundException());

        assertThrows(PersonNotFoundException.class, () -> personService.removeById(null));
    }

    @org.junit.jupiter.api.Test
    public void removeByName_sould_return_true() throws PersonNotFoundException {
        Person man = person;
        when(personDao.deleteByName(person.getName())).thenReturn(true);

        boolean deleted = personService.removeByName(person.getName());
        assertTrue(deleted);
    }

    @org.junit.jupiter.api.Test
    public void removeByName_sould_throw_PersonNotFoundExcepton_if_that_name_is_not_exist() throws PersonNotFoundException {
        personDao.savePerson(person);

        when(personDao.deleteByName("sdf")).thenThrow(new PersonNotFoundException());

        assertThrows(PersonNotFoundException.class, () -> personService.removeByName("sdf"));
    }

    @org.junit.jupiter.api.Test
    public void removeAll_sould_return_true() throws PersonNotFoundException {

        when(personDao.deleteAll()).thenReturn(true);

        personService.removeAll();

        verify(personDao, times(1)).deleteAll();


    }


}

