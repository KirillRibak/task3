package dao;

import entity.Person;
import exception.NotPersonException;
import exception.PersonNotFoundException;

import java.util.List;

public interface PersonDao {
    String savePerson(Person person);

    Person getPerson(String id) throws PersonNotFoundException, NotPersonException;

    List<String> saveAll(List<Person> personList) throws NullPointerException;

    List<Person> findAll() throws NotPersonException;

    List<Person> findByName(String name)throws PersonNotFoundException;

    List<Person> findByAge(byte age)throws PersonNotFoundException;

    boolean deleteById(String id) throws PersonNotFoundException;

    boolean deleteByName(String name)throws PersonNotFoundException;

    boolean deleteAll();

    List<Person> findByNameAndAge(String name,byte age)throws PersonNotFoundException;
}