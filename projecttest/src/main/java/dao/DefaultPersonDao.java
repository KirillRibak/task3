package dao;

import entity.Person;
import exception.NotPersonException;
import exception.PersonNotFoundException;
import storage.TemporaryStorage;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class DefaultPersonDao implements PersonDao {

    private TemporaryStorage storage = TemporaryStorage.getInstance();

    public DefaultPersonDao() {
    }

    public String savePerson(Person person) {
        String id = UUID.randomUUID().toString();
        person.setId(id);
        storage.insertEntity(id, person);
        return id;
    }

    public Person getPerson(String id) throws PersonNotFoundException, NotPersonException {
        Object entity = storage.getEntity(id);
        if (entity == null) {
            throw new PersonNotFoundException();
        }
        if (!Person.class.equals(entity.getClass())) {
            throw new NotPersonException();
        }
        return (Person) entity;
    }

    @Override
    public List<String> saveAll(List<Person> personList) throws NullPointerException {
        if (storage == null) {
            throw new NullPointerException();
        } else {
            List<String> listIds = new ArrayList<>();
            for (Person person : personList) {
                String id = savePerson(person);
                listIds.add(id);
            }
            return listIds;
        }
    }

    @Override
    public List<Person> findAll() throws NotPersonException {
        if (storage == null) {
            throw new NotPersonException();
        } else {
            List<Person> pers = new ArrayList<Person>();
            for (String id : storage.getKeys()) {
                pers.add((Person) storage.getEntity(id));
            }
            return pers;
        }
    }

    @Override
    public List<Person> findByName(String name) throws PersonNotFoundException {

        List<Person> pers = new ArrayList<Person>();
        for (Person person : storage.getValues()) {
            if (person.getName() == name) {
                pers.add(person);
            }
        }
        if (pers.isEmpty()) {
            throw new PersonNotFoundException();
        } else {
            return pers;
        }
    }

    @Override
    public List<Person> findByAge(byte age) throws PersonNotFoundException {

        List<Person> pers = new ArrayList<Person>();
        for (Person person : storage.getValues()) {
            if (person.getAge() == age) {
                pers.add(person);
            }
        }
        if (pers.isEmpty()) {
            throw new PersonNotFoundException();
        } else {
            return pers;
        }
    }

    @Override
    public boolean deleteById(String id) throws PersonNotFoundException {

        if (storage.contains(id)) {
            storage.remove(id);
            return true;
        } else {
            throw new PersonNotFoundException();
        }
    }

    @Override
    public boolean deleteByName(String name) throws PersonNotFoundException {
        for (String id : storage.getKeys()) {
            Person man = (Person) storage.getEntity(id);
            if (man.getName() == name) {

                storage.remove(id);
                return true;
            }
        }
        throw new PersonNotFoundException();
    }

    @Override
    public boolean deleteAll() {
        if (storage == null) {
            return true;
        } else {
            for (String id : storage.getKeys())
                storage.remove(id);
            return true;
        }
    }


    @Override
    public List<Person> findByNameAndAge(String name, byte age) throws PersonNotFoundException {
        List<Person> people = new ArrayList<Person>();
        for (Person person : storage.getValues()) {
            if (person.getAge() == age && person.getName() == name) {
                people.add(person);
            }
        }
        if (people.isEmpty()) {
            throw new PersonNotFoundException();
        } else {
            return people;
        }
    }
}
