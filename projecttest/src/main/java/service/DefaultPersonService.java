package service;

import dao.DefaultPersonDao;
import dao.PersonDao;
import dto.Employer;
import entity.Person;
import exception.NotPersonException;
import exception.PersonNotFoundException;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class DefaultPersonService implements PersonService{
    private PersonDao personDao = new DefaultPersonDao();

    @Override
    public UUID save(Employer employer){
        Person person = createPerson(employer);
        String id = personDao.savePerson(person);
        return  UUID.fromString(id);
    }

    private Person createPerson(Employer employer){
        return new Person(employer.getName(), (byte)employer.getAge());
    }

    @Override
    public Person get(UUID uuid){
        Person person = null;
        try{
            person = personDao.getPerson(uuid.toString());
        } catch (PersonNotFoundException | NotPersonException e){
            System.err.println("ERROR");
        }
        return person;
    }

    @Override
    public List<UUID> saveAll(List<Employer> employerList) {
        if (employerList == null) {
            throw new NullPointerException();
        }
        List<Person> personList = employerList.
                stream()
                .map(employer -> createPerson(employer))
                .collect(Collectors.toList());
        return personDao.saveAll(personList)
                .stream()
                .map(id -> UUID.fromString(id))
                .collect(Collectors.toList());
    }

    @Override
    public List<Person> getByName(String name) throws PersonNotFoundException {
        List<Person> people = personDao.findByName(name);
        return people.size() > 0 ? people : null;
    }

    @Override
    public List<Person> getByAge(int age) throws PersonNotFoundException {
        List<Person> people = personDao.findByAge((byte) age);
        return people.size() > 0 ? people : null;
    }

    @Override
    public List<Person> getByNameAndAge(String name, int age) throws PersonNotFoundException {
        List<Person> people = personDao.findByNameAndAge(name, (byte) age);
        return people.size() > 0 ? people : null;
    }

    @Override
    public boolean removeById(String id) throws PersonNotFoundException {
        return personDao.deleteById(id);
    }

    @Override
    public boolean removeByName(String name) throws PersonNotFoundException {
        return personDao.deleteByName(name);
    }

    @Override
    public void removeAll() {
        personDao.deleteAll();
    }

    @Override
    public List<Person> getAll() throws NotPersonException {
        List<Person> people = personDao.findAll();
        return people.size() > 0 ? people : null;
    }


}