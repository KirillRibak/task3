package service;

import dto.Employer;
import entity.Person;
import exception.NotPersonException;
import exception.PersonNotFoundException;

import java.util.List;
import java.util.UUID;

public interface PersonService {
    UUID save(Employer employer);

    Person get(UUID uuid);

    List<UUID> saveAll(List<Employer> employerList);

    List<Person> getAll() throws NotPersonException;

    List<Person> getByName(String name) throws PersonNotFoundException;

    List<Person> getByAge(int age) throws PersonNotFoundException;

    List<Person> getByNameAndAge(String name, int age) throws PersonNotFoundException;

    boolean removeById(String id) throws PersonNotFoundException;

    boolean removeByName(String name) throws PersonNotFoundException;

    void removeAll();

}