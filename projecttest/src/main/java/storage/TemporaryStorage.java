package storage;

import entity.Person;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class TemporaryStorage {
    private Map<String, Object> storage = new HashMap<String, Object>(256);

    private TemporaryStorage() {
    }

    public Object getEntity(String id) {
        return storage.get(id);
    }

    public List<String> getKeys(){
        List<String> keys=new ArrayList<String>();
        for(String key:storage.keySet()){
            keys.add(key);
        }
        return keys;
    }

    public void insertEntity(String id, Object entity) {
        storage.put(id, entity);
    }

    public List<Person> getValues(){
        List<Person> personList=new ArrayList<Person>();
        for(String id:storage.keySet()){
            personList.add((Person)storage.get(id));
        }
        return personList;
    }

    public boolean contains(String thisId){
        for (String id:storage.keySet()){
            if (id==thisId){
                return true;
            }
        }
        return false;
    }

    public void remove(String id){
        storage.remove(id);
    }

    public static TemporaryStorage getInstance() {
        return Holder.instance;
    }


    private static class Holder {
        private static TemporaryStorage instance = new TemporaryStorage();
    }


}